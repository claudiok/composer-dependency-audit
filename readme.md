Check composer dependencies for known vulnerabilities
====

```
# first, generate a json file with a list of dependencies:
composer show -l -f json > dependencies.json

# then, run this script to iterate over the dependencies,
# read the corresponding packist.org webiste, and check
# if there is a "advisory alert" for the version in use.
node check_deps.js dependencies.json
```

**Warning**: This script relies on the HTML structure that is
currently served by packagist.org. This may change.
**Don't rely on results of this script without checking if it
works correctly**. This can be done for example by using
a test file `test-deps.json`, with a vulnerable dependency.
The script, given this file, should show advisory alerts.
If it doesn't, then the websites have probably changed.

